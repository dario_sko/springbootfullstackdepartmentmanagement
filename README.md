# Inhaltsverzeichnis
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Was ist Springboot?](#was-ist-springboot)
- [Dependency Injection](#dependency-injection)
- [Spring Initializr](#spring-initializr)
- [SpringBootApplication (Main)](#springbootapplication-main)
- [Simple REST-API](#simple-rest-api)
- [Spring Boot DevTools](#spring-boot-devtools)
- [Architecture](#architecture)
- [Datenbank](#datenbank)
- [Entity](#entity)
- [DepartmentController](#departmentcontroller)
- [Insomnia](#insomnia)
- [JPA Methods](#jpa-methods)
- [Validation](#validation)
- [Logger](#logger)
- [Lombok](#lombok)
- [Exception Handling](#exception-handling)
    - [ExceptionHandler](#exceptionhandler)
- [Wechsel H2 → MySQL](#wechsel-h2--mysql)
- [Unit Testing - Mockito](#unit-testing---mockito)
  - [Service Layer Testing](#service-layer-testing)
  - [Repo Layer Testing](#repo-layer-testing)
  - [Controller Layer Testing](#controller-layer-testing)
- [Adding Config in properties file](#adding-config-in-properties-file)
- [Application.yml](#applicationyml)
- [Spring Boot Profiles](#spring-boot-profiles)
- [Deploy and Run SpringBoot](#deploy-and-run-springboot)
- [Spring Actuator](#spring-actuator)
  - [Endpoints erstellen](#endpoints-erstellen)

# Was ist Springboot?

Open-Source-Framework Spring das die die Komplexität der Java-Programmierung reduziert, Spring Boot ist ein Tool vom Framework. Neue Spring-Projekte können anhand von Spring Boot, einer Konvention der Konfiguration, stark vereinfacht entwickelt werden. Das Ergebnis ist ein Gerüst, das für Java-Applikationen bestens geeignet ist, die nicht von der ersten Code-Zeile an neu programmiert werden müssen.

# Dependency Injection

Die Kontrolle um Objekt zu erstellen werden dem Framework übergeben - Dependency Injection ist der Mechanisumus um dies zu aktivieren (zB @Autowired) → Inversion of Control ist ein Prinzip in der Softwareentwicklung, das die Kontrolle von Objekten oder Programmteilen auf einen Container oder ein Framework überträgt.

# Spring Initializr

[https://start.spring.io/](https://start.spring.io/) → Dependencies: Spring WEB, H2 Database

Generierter Ordnerstruktur in IDE öffnen → pom.xml

<img src="images/image1.png" width=600>

# SpringBootApplication (Main)

Annotation mit `@SpringBootApplication` ↔ `@Configuration`, `@EnableAutoConfiguration`, und `@ComponentScan`

Alle Konfiguration werden automatisch hinzugefügt.

Scannt alle Komponenten die für die Application notwendig sind

# Simple REST-API

Java Klasse erstellen und Annotation `@RestController` verwenden das Spring Boot erkennt das es sich um einen RestController handelt.

Weitere Notation gibt es: `@Controller` und `@Compenent`

`@RequestMapping*(*value = "/", method = RequestMethod.*GET)*` Um eine Anfrage einzustellen → Kürzere Form `@GetMapping("/")`

Es gibt auch @PostMapping, @DeleteMapping, etc.

# Spring Boot DevTools

Das man den Webserver nicht dauernd neustarten muss können diese DevTools verwendet werden, dass bei jeder Änderung automatisch im Hintergrund der Server neugestartet wird. 

Registry... Compiler.automake.allow.parallel

Settings→Build Tools → Compiler → Build project automatically

# Architecture

<img src="images/image2.png" width=600>

# Datenbank

H2 Parameter 

```xml
spring.h2.console.enabled=true
spring.datasource.url=jdbc:h2:mem:dcbapp
spring.datasource.driver-class-name=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=password
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
```

[http://localhost:8082/h2-console](http://localhost:8082/h2-console) aufrufbar

# Entity

Klasse erstellen → das die Datenbank weiß das es sich um eine Entity handel muss die Annotation `@Entity` verwendet werden

# DepartmentController

`@Autowired` auf DepartmentService um Kontrolle von Objekterzeugung zu übergeben

`@PathVariable` ****wird auf ein Methodenargument angewandt um den Wert einer URI Templatevariable auf das Attribut abzubilden. 

# Insomnia

Post - Senden wir ein JSON und wird in unsere Datenbank gespeichert

Get - Erhalten wir ein JSON

Delete - Kann zB per Id gelöscht werden

Put - Daten ändern

# JPA Methods

[https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation)

# Validation

Die Dependency

```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-validation</artifactId>
</dependency>
```

 Danach könnne die Annotation verwendet werden

`@NotBlank*(*message = "Please Add Department Name"*)` →* Darf nicht leer sein sonst gibt es diese Nachricht aus

Weitere Validation Annotation:

```
@Length(max = 5,min =1)
@Size(max = 10, min = 0)
@Email
@Positive
@Negative
@PositiveOrZero
@NegativeOrZero
@Future
@FutureOrPresent
@Past
@PastOrPresent
```

# Logger

Logger erstellen

```
private final Logger LOGGER =
        LoggerFactory.getLogger(DepartmentController.class);
```

Logger Info speichern für Protokollierung

```xml
LOGGER.info*(*"Inside saveDepartment of DepartmentConroller"*)*;
```

# Lombok

generiert alle Getter und Setter oder Konstruktore mit einer Annotation

```java
@Data // erstellt alles Getter/Setter/RequiredArgsConstruct/ToString
@Getter // erstellt nur Getter
@Setter // erstellt nur Setter
@NoArgsConstructor // Default/Leerer Konstruktor
@AllArgsConstructor // Konstruktor mit allen Parametern
```

# Exception Handling

extends Exception alle Override Methods generieren (Alt + Einfg)

ServiceImpl Exception throwen → Service Interface Exception in Method Signature und in Controller throws in Methode

### ExceptionHandler

extends ResponseEntityExceptionHandler

Für die Klasse braucht es diese Annotation @ControllerAdvice → Eine zentrale Möglichkeit, Ausnahmen, Bindungen usw. zu behandeln, die für alle definierten Controller gelten.

Die Methoden brauchen die Annotation @ExceptionHandler

```java
@ControllerAdvice
@ResponseStatus
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(DepartmentNotFoundException.class)
    public ResponseEntity<ErrorMessage> departmentNotFoundException(DepartmentNotFoundException exception,
                                                                    WebRequest request) {
        ErrorMessage message = new ErrorMessage(HttpStatus.NOT_FOUND, exception.getMessage());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
    }
```

# Wechsel H2 → MySQL

application.properties

```java
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://192.168.227.21:3306/departmentdb
spring.datasource.username=root
spring.datasource.password=123
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.show-sql: true
```

pom.xml

```java
				<dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.25</version>
        </dependency>
```

# Unit Testing - Mockito

Mit dem Framework „Mockito“ kann man ein komplexes Objekt faken, bzw. Methoden und Objekte innerhalb einer Klasse nachbauen und für einen Testfall mit bestimmten Werten zurückgeben.

## Service Layer Testing

Klasse - Generate - Test - Before ankreuzen

```java
@SpringBootTest
class DepartmentServiceTest {

@Autowired
private DepartmentService departmentService;
@MockBean
private DepartmentRepository departmentRepository;

// Vor jedem Test wird diese Methode ausgeführt
@BeforeEach
    void setUp() {
		>>> datenbankbefüllen <<<
}

@Test
@DisplayName("Get Data based on Valida Department Name")
    public void whenValidDepartmentName_thenDepartmentShouldFound() {
        String departmentName = "IT";
        Department found =
                departmentService.fetchDeparmentByName(departmentName);

        assertEquals(departmentName, found.getDepartmentName());
    }
```

## Repo Layer Testing

```java
@DataJpaTest // Annotation für JPA Test - Um Tests mit einer eingebettete In-Memory-Datenbank ausführen zu können
// Somit werden keine Daten in die Original Datenbank gespeichert
class DepartmentRepositoryTest {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private TestEntityManager testEntityManager;

		// Vor jedem Test wird diese Methode ausgeführt
    @BeforeEach
    void setUp() {
        Department department =
                Department.builder()
                        .departmentName("Mechanical")
                        .departmentCode("ME011")
                        .departmentAddress("Imst")
                        .build();
        testEntityManager.persist(department);
    }

    @Test
    public void whenFindById_thenReturnDepartment() {
        Department department = departmentRepository.findById(1L).get();
        assertEquals(department.getDepartmentName(), "Mechanical");
    }
```

## Controller Layer Testing

```java
@WebMvcTest(DepartmentController.class)
class DepartmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DepartmentService departmentService;

    private Department department;

    @BeforeEach
    void setUp() {
        department = Department.builder()
                .departmentAddress("LangeStrasse")
                .departmentCode("Code1")
                .departmentName("Dario")
                .departmentId(1L)
                .build();
    }

    @Test
    void saveDepartment() throws Exception {
        Department inputDepartment = Department.builder()
                .departmentAddress("LangeStrasse")
                .departmentCode("Code1")
                .departmentName("Dario")
                .build();

        Mockito.when(departmentService.saveDepartment(inputDepartment))
                .thenReturn(department);

        mockMvc.perform(MockMvcRequestBuilders.post("/departments")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"departmentName\":\"Dario\",\n" +
                                "\t\"departmentAddress\":\"LangeStrasse\",\n" +
                                "\t\"departmentCode\":\"Code1\"\n" +
                                "}"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void fetchDepartmentById() throws Exception {
        Mockito.when(departmentService.fetchDeparmentById(1L)).thenReturn(department);

        mockMvc.perform(get("/departments/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.departmentName").
                        value(department.getDepartmentName()));
    }
}
```

# Adding Config in properties file

Application.properties

```java
welcome.message=Hello World!!
```

HelloContoller

```java
@RestController
public class HelloController {

    @Value("${welcome.message}")
    private String welcomeMessage;

    @GetMapping("/")
    public String helloWorld() {
        return welcomeMessage;
    }

}
```

# Application.yml

Nützlich um die Konfig leichter lesen zu können als in application.properties → application.properties kann auskommentiert werden

Können umkonvertiert werden: [http://mageddo.com/tools/yaml-converter](http://mageddo.com/tools/yaml-converter)

# Spring Boot Profiles

Um verschiedene Config Profile benutzen zu können müssen mehrere in der File erstellt werden und vermerkt werden welches aktiv genützt wird

# Deploy and Run SpringBoot

Version kann in der pom.xml geändert werden

```java
<version>0.0.1-SNAPSHOT</version>
```

Danach muss ein .jar Datei generiert werden mit Maven - clean und dann install - im target Ordner liegt die erstellte jar Datei

Mit diesem Befehl kann die jar Datei ausgeführt werden - `—spring.profiles.active=<profilename>` welches Config-Profile sollte benutzt werden

```bash
java -jar .\Spring-Boot-Department-0.0.1-SNAPSHOT.jar --spring.profiles.active=prod
```

# Spring Actuator

 Monitoring und Manage Application

```bash
<dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

Danach können die Endpoints auf dieser Seite gefunden werden: [http://localhost:8082/actuator](http://localhost:8082/actuator)

Um mehrere Endpoints hinzuzufügen muss in der yml Datei dieses Config erstellt werden

```bash
management:
  endpoints:
    web:
      exposure:
        include: "*"
        exclude: "env,beans" # Um Endpoints auszuschließen
```

## Endpoints erstellen

```java
@Component
@Endpoint(id = "features")
public class FeatureEndpoint {

    private final Map<String, Feature> featureMap =
            new ConcurrentHashMap<>();

    public FeatureEndpoint() {
        featureMap.put("Department", new Feature(true));
        featureMap.put("User", new Feature(false));
        featureMap.put("Authentication", new Feature(false));
    }

    @ReadOperation
    public Map<String, Feature> features() {
        return featureMap;
    }

    @ReadOperation
    public Feature feature(@Selector String featureName) {
        return featureMap.get(featureName);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    private static class Feature {
        private boolean isEnabled;
    }
}
```