package com.springboot.SpringBootDepartment.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.springboot.SpringBootDepartment.View.View;
import com.springboot.SpringBootDepartment.entity.Employee;
import com.springboot.SpringBootDepartment.error.DepartmentNotFoundException;
import com.springboot.SpringBootDepartment.error.EmployeeNotFoundException;
import com.springboot.SpringBootDepartment.service.EmployeeDTO;
import com.springboot.SpringBootDepartment.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @PostMapping("/employee")
    public Employee saveEmployee(@RequestBody EmployeeDTO employeeDTO) throws DepartmentNotFoundException {
        return employeeService.saveEmployee(employeeDTO);
    }

    @JsonView(View.User.class)
    @GetMapping("/employee/user")
    public List<Employee> getAllEmployeeUser() {
        return employeeService.getAllEmployee();
    }

    @JsonView(View.Admin.class)
    @GetMapping("/employee/admin")
    public List<Employee> getAllEmployeeAdmin() {
        return employeeService.getAllEmployee();
    }

    @GetMapping("/employee/user/all")
    public List<Employee> getAllEmployeeUserAll() {
        return employeeService.getAllEmployee();
    }

    @GetMapping("/employee/{id}")
    public Employee getEmployeeById(@PathVariable("id") Long id) throws EmployeeNotFoundException {
        return employeeService.getEmployeeById(id);
    }

    @PutMapping("/employee/{id}")
    public Employee changeEmployeePerId(@RequestBody Employee employee) {
        return employeeService.changeEmployeePerId(employee);
    }

    @DeleteMapping("/employee/{id}")
    public String deleteEmployeePerId(@PathVariable("id") Long id) {
        employeeService.deleteEmployePerId(id);
        return "Employee deleted";
    }
}
