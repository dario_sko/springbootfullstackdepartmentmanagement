package com.springboot.SpringBootDepartment.entity;

import com.fasterxml.jackson.annotation.JsonView;
import com.springboot.SpringBootDepartment.View.View;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Department {

    @Id
    @SequenceGenerator(
            name = "department_sequence",
            sequenceName = "department_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "department_sequence"
    )
    private Long departmentId;

    @JsonView(View.User.class)
    @NotBlank(message = "Please Add Department Name")
    private String departmentName;
    @JsonView(View.Admin.class)
    private String departmentAddress;
    private String departmentCode;

}
