package com.springboot.SpringBootDepartment.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonView;
import com.springboot.SpringBootDepartment.View.View;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Employee {

    @Id
    @SequenceGenerator(
            name = "employee_sequence",
            sequenceName = "employee_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "employee_sequence"
    )
    @JsonView({View.User.class})
    private Long employeeId;

    @JsonView({View.User.class})
    @NotBlank(message = "Please add a First Name")
    private String employeeFirstName;
    @JsonView(View.Admin.class)
    private String employeeLastName;

    @ManyToOne(
            cascade = CascadeType.ALL,
            optional = false
    )
    @JoinColumn(
            name = "department_id",
            referencedColumnName = "departmentId"
    )
    @JsonUnwrapped
    @JsonView(View.User.class)
    private Department department;
}
