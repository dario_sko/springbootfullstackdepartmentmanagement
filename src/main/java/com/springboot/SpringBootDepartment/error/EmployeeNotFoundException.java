package com.springboot.SpringBootDepartment.error;

public class EmployeeNotFoundException extends Exception {

    public EmployeeNotFoundException() {
    }

    // Um StackTrace nicht auszugeben!!
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public EmployeeNotFoundException(String message) {
        super(message);

    }

    public EmployeeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmployeeNotFoundException(Throwable cause) {
        super(cause);
    }

    public EmployeeNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
