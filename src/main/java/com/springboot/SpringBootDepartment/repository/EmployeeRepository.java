package com.springboot.SpringBootDepartment.repository;

import com.springboot.SpringBootDepartment.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Employee getEmployeeByEmployeeId(Long id);



}