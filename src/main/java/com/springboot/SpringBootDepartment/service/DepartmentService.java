package com.springboot.SpringBootDepartment.service;

import com.springboot.SpringBootDepartment.entity.Department;
import com.springboot.SpringBootDepartment.error.DepartmentNotFoundException;

import java.util.List;

public interface DepartmentService {
    public Department saveDepartment(Department department);

    public List<Department> fetchDeparmentList();

    public Department fetchDeparmentById(Long departmentId) throws DepartmentNotFoundException;

    public void deleteDepartmentById(Long departmentId);

    public Department updateDepartment(Long departmentId, Department department);

    public Department fetchDeparmentByName(String departmentName);
}
