package com.springboot.SpringBootDepartment.service;

import com.springboot.SpringBootDepartment.entity.Department;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeDTO {

    private String employeeFirstName;
    private String employeeLastName;
    private Long departmentId;
}