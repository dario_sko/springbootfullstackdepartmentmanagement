package com.springboot.SpringBootDepartment.service;

import com.springboot.SpringBootDepartment.entity.Employee;
import com.springboot.SpringBootDepartment.error.DepartmentNotFoundException;
import com.springboot.SpringBootDepartment.error.EmployeeNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

public interface EmployeeService {
    Employee saveEmployee(EmployeeDTO employeeDTO) throws DepartmentNotFoundException;

    List<Employee> getAllEmployee();

    Employee getEmployeeById(Long id) throws EmployeeNotFoundException;

    Employee changeEmployeePerId(Employee employee);

    void deleteEmployePerId(Long id);
}
