package com.springboot.SpringBootDepartment.service;

import com.springboot.SpringBootDepartment.entity.Department;
import com.springboot.SpringBootDepartment.entity.Employee;
import com.springboot.SpringBootDepartment.error.DepartmentNotFoundException;
import com.springboot.SpringBootDepartment.error.EmployeeNotFoundException;
import com.springboot.SpringBootDepartment.repository.DepartmentRepository;
import com.springboot.SpringBootDepartment.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private DepartmentRepository departmentRepository;


    @Override
    public Employee saveEmployee(EmployeeDTO employeeDTO) throws DepartmentNotFoundException {
        Optional<Department> department = departmentRepository.findById(employeeDTO.getDepartmentId());
        if (!department.isPresent()) {
            throw new DepartmentNotFoundException();
        }

        Employee employee = Employee.builder()
                .employeeFirstName(employeeDTO.getEmployeeFirstName())
                .employeeLastName(employeeDTO.getEmployeeLastName())
                .department(department.get())
                .build();
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee getEmployeeById(Long id) throws EmployeeNotFoundException {
        Optional<Employee> employee = employeeRepository.findById(id);
        if (!employee.isPresent()) {
            throw new EmployeeNotFoundException("Employee not found!");
        }
        return employeeRepository.getEmployeeByEmployeeId(id);
    }

    @Override
    public Employee changeEmployeePerId(Employee employee) {
        Department oldDepartment = employeeRepository.getEmployeeByEmployeeId(employee.getEmployeeId()).getDepartment();

        Employee newEmployee = Employee.builder()
                .employeeId(employee.getEmployeeId())
                .employeeFirstName(employee.getEmployeeFirstName())
                .employeeLastName(employee.getEmployeeLastName())
                .department(oldDepartment)
                .build();

        return employeeRepository.save(newEmployee);

    }

    @Override
    public void deleteEmployePerId(Long id) {
        employeeRepository.deleteById(id);
    }


}
