package com.springboot.SpringBootDepartment.controller;

import com.springboot.SpringBootDepartment.entity.Department;
import com.springboot.SpringBootDepartment.entity.Employee;
import com.springboot.SpringBootDepartment.service.EmployeeDTO;
import com.springboot.SpringBootDepartment.service.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.ServletContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmployeeController.class)
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    private Employee employee;
    private Department department;

    @BeforeEach
    void setUp() {
        this.department = Department.builder()
                .departmentAddress("LangeStrasse")
                .departmentCode("Code1")
                .departmentName("Dario")
                .departmentId(1L)
                .build();

        this.employee = Employee.builder()
                .employeeFirstName("Dario")
                .employeeLastName("Sko")
                .employeeId(1L)
                .department(this.department)
                .build();


    }

    @Test
    public void getEmployeeById() throws Exception {
        Mockito.when(employeeService.getEmployeeById(1L)).thenReturn(employee);

        mockMvc.perform(get("/employee/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.employeeFirstName").
                        value(employee.getEmployeeFirstName()));

    }
}