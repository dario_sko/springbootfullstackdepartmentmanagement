package com.springboot.SpringBootDepartment.repository;

import com.springboot.SpringBootDepartment.entity.Department;
import com.springboot.SpringBootDepartment.entity.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class EmployeeRepositoryTest {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @BeforeEach
    void setUp() {
        Department department = Department.builder()
                .departmentAddress("LangeStrasse")
                .departmentCode("Code1")
                .departmentName("Dario")
                .departmentId(1L)
                .build();

        // Funktioniert mit merge aber nicht mit persist
        testEntityManager.merge(department);

        Employee employee = Employee.builder()
                .employeeFirstName("Dario")
                .employeeLastName("Sko")
                .employeeId(1L)
                .department(department)
                .build();

        testEntityManager.merge(employee);
    }

    @Test
    public void findDepartmentByDepartmentNameTest() {

        Department department = employeeRepository.getEmployeeByEmployeeId(1L).getDepartment();
        assertEquals("Dario", department.getDepartmentName());
    }
}