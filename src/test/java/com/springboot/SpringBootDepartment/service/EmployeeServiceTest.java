package com.springboot.SpringBootDepartment.service;

import com.springboot.SpringBootDepartment.entity.Department;
import com.springboot.SpringBootDepartment.entity.Employee;
import com.springboot.SpringBootDepartment.error.EmployeeNotFoundException;
import com.springboot.SpringBootDepartment.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;

    @MockBean
    private EmployeeRepository employeeRepository;


    @BeforeEach
    void setUp() {
        Department department = Department.builder()
                .departmentAddress("LangeStrasse")
                .departmentCode("Code1")
                .departmentName("Dario")
                .departmentId(1L)
                .build();

        Employee employee = Employee.builder()
                .employeeFirstName("Dario")
                .employeeLastName("Sko")
                .employeeId(1L)
                .department(department)
                .build();

        Mockito.when(employeeRepository.getEmployeeByEmployeeId(1L)).thenReturn(employee);

    }

    @Test
    @DisplayName("Get Data based on Valida Employee Name")
    public void whenValidFirstName_thenEmployeeShouldFound() throws EmployeeNotFoundException {
        String firstName = "Dario";
        Employee employeeFound = employeeService.getEmployeeById(1L);

        assertEquals(firstName, employeeFound.getEmployeeFirstName());
    }


}